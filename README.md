# Macrophage


[![Pipeline Status](https://gitlab.com/tonytyno/macrophage/badges/main/pipeline.svg)](https://gitlab.com/tonytyno/macrophage/pipelines)
[![coverage report](https://gitlab.com/tonytyno/macrophage/badges/main/coverage.svg)](https://gitlab.com/tonytyno/macrophage/commits/main)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

[Documentation](https://gitlab.com/tonytyno/macrophage/README.md) - [Repository](https://gitlab.com/tonytyno/macrophage) - [Issues](https://gitlab.com/tonytyno/macrophage/-/issues)


## About Macrophage
Macrophage, by [Linaro](https://www.linaro.org/), is a command line tool for scraping LAVA website instances and search for dead links or word patterns 


## Installing Macrophage 

### Running uninstalled


!!! note
    Macrophage requires Python 3.6 or newer.

Run it directly from the source directory. 
Get the sources via git or something else.

```shell
/path/to/macrophage/python3 -m macrophage --help
```

## Using Macrophage


How to use Macrophage:

1. crawl mode - crawl selected website
```shell
python3 -m macrophage crawl <lava_url>
```

Set custom http response status codes to be searched
```shell
python3 -m macrophage crawl <lava_url> --config 404 500 
```

2. after crawling you are able to print dead links or generate report

Print results in console
```shell
python3 -m macrophage link  <lava_url> --print
```

Print number of records from database table
```shell
python3 -m macrophage link <lava_url> --summary
```

Generate report file (available file types: yaml, json, csv, txt)
```shell
python3 -m macrophage link <lava_url> --report-out path/to/file.json
```

3. after crawl you can search crawled website for searched term

```shell
python3 -m macrophage search <lava_url> <searched_word> 
```

Print results in console
```shell
python3 -m macrophage search  <lava_url> <searched_word> --print
```

Print number of records from database table
```shell
python3 -m macrophage search <lava_url> <searched_word> --summary
```

Generate report file (available file types: yaml, json, csv, txt)
```shell
python3 -m macrophage search <lava_url> <searched_word> --report-out path/to/file.json
```

4. additional settings

Set custom path for log file
```shell
--log-file path/to/file.txt
```

Set custom log level (info, debug, warn, error)
```shell
--log-level
```

Set custom path to sqlite database
```shell
--db path/to/database.db
```

Print current version
```shell
-v
```

Macrophage will automatically start.
