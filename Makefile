export PROJECT := macrophage
#export TUXPKG_MIN_COVERAGE := 80

#include $(shell tuxpkg get-makefile)

style:
	black --check --diff .

flake8:
	flake8 .

htmlcov:
	python3 -m pytest --cov=macrophage --cov-report=html

stylecheck: style flake8 

typecheck:
	mypy .

spellcheck:
	codespell \
		--check-filenames \
		--skip '.git,public,dist,*.sw*,*.pyc,tags,*.json,.coverage,htmlcov,*.jinja2,.venv'


#doc: docs/index.md
#	mkdocs build

#docs/index.md: README.md scripts/readme2index.sh
#	scripts/readme2index.sh $@

#doc-serve:
#	mkdocs serve


