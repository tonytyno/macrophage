import pytest

from unittest import mock
from scrapy.http import Request, Response
from macrophage.crawler import (
    CrawlingSpider,
    trim_allowed_domains,
    trim_urls_to_domains,
)

from scrapy.crawler import CrawlerProcess
from pathlib import Path

BASE = (Path(__file__) / "..").resolve()
TEST_FILE = BASE / "test_files/test-html.html"
URL = "http://example.com"

spider_kwargs = {
    "db_path": ":memory:",
    "start_urls": TEST_FILE,
    "domains": ["example.com"],
    "allowed_domains": ["example.com"],
    "response_list": [404, 500],
    "handle_httpstatus_list": [404, 500],
}


@mock.patch("macrophage.crawler.CrawlingSpider.crawl_parse")
def test_crawler(mock_crawler):
    request = Request(url=URL)
    file_content = open(TEST_FILE, "rb").read()
    response = Response(url=URL, request=request, body=file_content)
    response.encoding = "utf-8"
    fake_response = Response(url=URL, request=request, body=file_content)
    mock_crawler.return_value = fake_response
    test_spider = CrawlerProcess()
    test_spider.crawl(CrawlingSpider, **spider_kwargs)
    test_spider.start()


def test_trim_allowed_domains_none():
    with pytest.raises(Exception) as exc:
        trim_allowed_domains(None)
    assert "Domain list is empty" in str(exc.value)


def test_trim_allowed_domains():
    assert trim_allowed_domains(
        ["http://0.0.0.0:8000", "https://validation.linaro.org"]
    ) == ["0.0.0.0", "validation.linaro.org"]


def test_trim_urls_domains_none():
    with pytest.raises(Exception) as exc:
        trim_urls_to_domains(None)
    assert "Url list is empty" in str(exc.value)


def test_trim_urls_domains():
    assert trim_urls_to_domains(
        ["http://0.0.0.0:8000", "https://validation.linaro.org"]
    ) == ("0.0.0.0:8000", "validation.linaro.org")
