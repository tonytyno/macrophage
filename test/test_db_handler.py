import sqlite3
import os
import unittest
import pytest
from pathlib import Path
from macrophage.db_handler import (
    db_init_responses,
    db_init_patterns,
    db_insert_response_value,
    db_print_results,
    db_init_body,
    db_dict_urls,
    db_print_summary,
    db_insert_body_value,
    db_pattern_find,
)


BASE = (Path(__file__) / "..").resolve()
TEST_DB_PATH = BASE / "test_files/test.db"


DATA_LIST = [
    ("https://linaro.org/example", "<html><title>Hello!</title></html>", "linaro.org"),
    (
        "https://linaro.org/example-next",
        "<html><title>Hello Again!</title></html>",
        "linaro.org",
    ),
]


class TestDB(unittest.TestCase):
    def setUp(self):

        self.body = db_init_body(TEST_DB_PATH)
        self.pattern = db_init_patterns(TEST_DB_PATH)
        self.response = db_init_responses(TEST_DB_PATH)

    def test_db_init_responses(self):
        db_con = sqlite3.connect(TEST_DB_PATH)
        with db_con:
            cursor = db_con.cursor()
            result = cursor.execute(
                """SELECT COUNT(*) FROM sqlite_master WHERE type='table' AND name='link';"""
            )
            assert len(result.fetchall()) > 0

    def test_db_init_responses_exception(self):
        db_con = sqlite3.connect(TEST_DB_PATH)
        with pytest.raises(Exception) as exc:
            with db_con:
                cursor = db_con.cursor()
                cursor.execute(
                    """SELECT COUNT(*) FROM sqlite_master WHERE type='table' AND name='link2';"""
                )
            assert "Macrophage execution stopped" in str(exc.value)

    def test_db_init_patterns(self):
        db_con = sqlite3.connect(TEST_DB_PATH)
        with db_con:
            cursor = db_con.cursor()
            result = cursor.execute(
                """SELECT COUNT(*) FROM sqlite_master WHERE type='table' AND name='search';"""
            )
            assert len(result.fetchall()) > 0

    def test_db_init_patterns_exception(self):
        db_con = sqlite3.connect(TEST_DB_PATH)
        with pytest.raises(Exception) as exc:
            with db_con:
                cursor = db_con.cursor()
                cursor.execute(
                    """SELECT COUNT(*) FROM sqlite_master WHERE type='table' AND name='search2';"""
                )
            assert "Macrophage execution stopped" in str(exc.value)

    def test_db_init_body(self):
        db_con = sqlite3.connect(TEST_DB_PATH)
        with db_con:
            cursor = db_con.cursor()
            result = cursor.execute(
                """SELECT COUNT(*) FROM sqlite_master WHERE type='table' AND name='crawl';"""
            )
            assert len(result.fetchall()) > 0

    def test_db_init_body_exception(self):
        db_con = sqlite3.connect(TEST_DB_PATH)
        with pytest.raises(Exception) as exc:
            with db_con:
                cursor = db_con.cursor()
                cursor.execute(
                    """SELECT COUNT(*) FROM sqlite_master WHERE type='table' AND name='crawl2';"""
                )
            assert "Macrophage execution stopped" in str(exc.value)

    def test_db_insert_response_value(self):
        db_insert_response_value(
            TEST_DB_PATH, "https://linaro.org", "200", "linaro.org"
        )
        db_con = sqlite3.connect(TEST_DB_PATH)
        with db_con:
            cursor = db_con.cursor()
            result = cursor.execute("SELECT * FROM 'link';")
            assert (1, "https://linaro.org", "200", "linaro.org") in result.fetchall()

    def test_db_insert_response_exception(self):

        db_insert_response_value(
            TEST_DB_PATH, "https://linaro.org", "201", "linaro.org"
        )
        db_con = sqlite3.connect(TEST_DB_PATH)
        with db_con:
            cursor = db_con.cursor()
            result = cursor.execute("SELECT * FROM 'link';")
            assert (
                2,
                "https://linaro.org",
                "201",
                "linaro.org",
            ) not in result.fetchall()

    def test_db_insert_body_value(self):
        db_insert_body_value(TEST_DB_PATH, DATA_LIST)
        db_con = sqlite3.connect(TEST_DB_PATH)
        with db_con:
            cursor = db_con.cursor()
            result = cursor.execute("SELECT * FROM 'crawl';")
            fetched_data = result.fetchall()
        assert "https://linaro.org/example" in fetched_data[0]
        assert len(fetched_data) == 2

    def test_db_dict_urls(self):
        db_insert_response_value(
            TEST_DB_PATH, "https://linaro.org/example3", "404", "linaro.org"
        )
        result = db_dict_urls(TEST_DB_PATH, ["linaro.org"])
        assert {
            "id": 1,
            "url": "https://linaro.org/example3",
            "status": "404",
            "domain": "linaro.org",
        } in result

    def test_db_dict_exception(self):
        with pytest.raises(Exception) as exc:
            db_dict_urls(TEST_DB_PATH, "0.0.0.0:8000")
        assert "Macrophage execution stopped" in str(exc.value)

    def test_db_pattern_find(self):
        db_insert_body_value(TEST_DB_PATH, DATA_LIST)
        result = db_pattern_find(TEST_DB_PATH, "Hello", ["linaro.org"])
        assert "https://linaro.org/example" in result[0]["url"]

    def test_db_pattern_exception(self):
        with pytest.raises(Exception) as exc:
            db_pattern_find(TEST_DB_PATH, "example", "0.0.0.0:8000")
        assert "Not able to search through database for selected word: example" in str(
            exc.value
        )

    def test_db_print_exception(self):
        with pytest.raises(Exception) as exc:
            db_print_results(TEST_DB_PATH, "link", "0.0.0.0:8000", [500, 404])
        assert "Macrophage execution stopped" in str(exc.value)

    #    def test_db_print(self):
    # db_print_results(TEST_DB_PATH, "link", ["linaro.org"])
    # out, err = capsys.readouterr()
    # assert "url: https://linaro.org http response: 200" in out

    def test_db_summary(self):
        db_print_summary(TEST_DB_PATH, "crawl", ["0.0.0.0:8000"])

    def test_db_summary_exception(self):
        with pytest.raises(Exception) as exc:
            db_print_summary(TEST_DB_PATH, "crawl", "0.0.0.0:8000")
        assert "Macrophage execution stopped" in str(exc.value)

    def tearDown(self):
        os.remove(TEST_DB_PATH)
