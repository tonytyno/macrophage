import sys
import pytest
import macrophage.__main__ as main_module


@pytest.fixture
def argv():
    return ["macrophage"]


@pytest.fixture(autouse=True)
def patch_argv(monkeypatch, argv):
    monkeypatch.setattr(sys, "argv", argv)


class TestMain:
    def test_start(self, monkeypatch, mocker):
        monkeypatch.setattr(main_module, "__name__", "__main__")
        main = mocker.patch("macrophage.__main__.main", return_value=1)
        exit = mocker.patch("sys.exit")
        main_module.start()
        main.assert_called()
        exit.assert_called_with(1)

    def test_main_version(self, argv, capsys):
        argv.append("-v")
        with pytest.raises(SystemExit):
            main_module.main()
        out, out_err = capsys.readouterr()
        assert "Macrophage" in out

    def test_main_help(self, capsys):
        main_module.main()
        out, out_err = capsys.readouterr()
        assert "LAVA Crawler" in out

    def test_main_crawl(self, argv, monkeypatch, mocker):
        argv.extend(["crawl", "http://0.0.0.0:8000"])
        monkeypatch.setattr(main_module, "__name__", "__main__")
        main_db_body = mocker.patch("macrophage.__main__.db_init_body")
        main_db_responses = mocker.patch("macrophage.__main__.db_init_responses")
        main_crawler_process = mocker.patch("macrophage.__main__.CrawlerProcess")
        exit = mocker.patch("sys.exit")
        main_module.start()
        main_db_body.assert_called()
        main_db_responses.assert_called()
        main_crawler_process.assert_called()
        exit.assert_called_with(0)

    def test_main_search(self, argv, monkeypatch, mocker):
        argv.extend(["search", "http://0.0.0.0:8000", "searched_word"])
        monkeypatch.setattr(main_module, "__name__", "__main__")
        main_db_patterns = mocker.patch("macrophage.__main__.db_init_patterns")
        main_db_pattern_find = mocker.patch("macrophage.__main__.db_pattern_find")
        exit = mocker.patch("sys.exit")
        main_module.start()
        main_db_patterns.assert_called()
        main_db_pattern_find.assert_called()
        exit.assert_called_with(0)

    def test_main_link(self, argv, monkeypatch, mocker):
        argv.extend(["link", "http://0.0.0.0:8000", "--report-out=file.txt"])
        monkeypatch.setattr(main_module, "__name__", "__main__")
        main_db_dict_urls = mocker.patch("macrophage.__main__.db_dict_urls")
        main_generate_raport = mocker.patch("macrophage.__main__.generate_raport")
        exit = mocker.patch("sys.exit")
        main_module.start()
        main_db_dict_urls.assert_called()
        main_generate_raport.assert_called()
        exit.assert_called_with(0)

    def test_main_search_report(self, argv, monkeypatch, mocker):
        argv.extend(
            ["search", "http://0.0.0.0:8000", "searched_word", "--report-out=file.txt"]
        )
        monkeypatch.setattr(main_module, "__name__", "__main__")
        main_generate_raport = mocker.patch("macrophage.__main__.generate_raport")
        main_db_patterns = mocker.patch("macrophage.__main__.db_init_patterns")
        main_db_pattern_find = mocker.patch("macrophage.__main__.db_pattern_find")
        exit = mocker.patch("sys.exit")
        main_module.start()
        main_db_patterns.assert_called()
        main_db_pattern_find.assert_called()
        main_generate_raport.assert_called()
        exit.assert_called_with(0)

    def test_main_summary(self, argv, monkeypatch, mocker):
        argv.extend(["link", "http://0.0.0.0:8000", "--summary"])
        monkeypatch.setattr(main_module, "__name__", "__main__")
        main_db_print_summary = mocker.patch("macrophage.__main__.db_print_summary")
        exit = mocker.patch("sys.exit")
        main_module.start()
        main_db_print_summary.assert_called()
        exit.assert_called_with(0)

    def test_main_print(self, argv, monkeypatch, mocker):
        argv.extend(["search", "http://0.0.0.0:8000", "searched_word", "--print"])
        monkeypatch.setattr(main_module, "__name__", "__main__")
        main_db_print_results = mocker.patch("macrophage.__main__.db_print_results")
        exit = mocker.patch("sys.exit")
        main_module.start()
        main_db_print_results.assert_called()
        exit.assert_called_with(0)
