import os
from pathlib import Path

from macrophage.raport_creator import generate_raport

EXAMPLE_DATA = [
    {
        "id": "1",
        "url": "http://linaro.org",
        "status": "200",
        "domain": "linaro.org",
    },
    {
        "id": "2",
        "url": "http://linaro.org/404.html",
        "status": "404",
        "domain": "linaro.org",
    },
]

BASE = (Path(__file__) / "..").resolve()
TEST_FILE = BASE / "test_files/report.txt"


def test_generate_raport():

    assert generate_raport(BASE / "test_files/report.txt", EXAMPLE_DATA) == 0
    assert generate_raport(BASE / "test_files/report.yaml", EXAMPLE_DATA) == 0
    assert generate_raport(BASE / "test_files/report.csv", EXAMPLE_DATA) == 0
    assert generate_raport(BASE / "test_files/report.json", EXAMPLE_DATA) == 0
    assert generate_raport(BASE / "test_files/report.xml", EXAMPLE_DATA) == 0
    assert generate_raport(None, EXAMPLE_DATA) == 1
    assert generate_raport(TEST_FILE, None) == 1
    assert generate_raport(TEST_FILE / "not/existing/path/report.txt", EXAMPLE_DATA)


def test_if_xml_is_present():
    assert os.path.isdir(BASE / "test_files/report.xml") is False


def test_if_files_created():
    assert os.path.exists(BASE / "test_files/report.txt") is True
    assert os.path.exists(BASE / "test_files/report.json") is True
    assert os.path.exists(BASE / "test_files/report.csv") is True
    assert os.path.exists(BASE / "test_files/report.yaml") is True
