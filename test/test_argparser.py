#
# Copyright 2022-present Linaro Limited
#
# SPDX-License-Identifier: MIT

import subprocess

from macrophage.__init__ import __version__


def test_parser():
    version_result = subprocess.run(
        ["python3", "-m", "macrophage", "-v"], stdout=subprocess.PIPE, text=True
    )
    help_result = subprocess.run(
        ["python3", "-m", "macrophage", "--help"], stdout=subprocess.PIPE, text=True
    )
    assert f"Macrophage, {__version__}" in version_result.stdout
    assert "usage: Macrophage [-h] [-v] {crawl,link,search} ..." in help_result.stdout


def test_subparser():
    subparser_result = subprocess.run(
        ["python3", "-m", "macrophage", "link", "-h"], stdout=subprocess.PIPE, text=True
    )
    assert (
        "usage: Macrophage link [-h] [--log-file LOG_FILE]" in subparser_result.stdout
    )
