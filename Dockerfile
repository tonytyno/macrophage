FROM debian:bullseye-slim

COPY requirements.txt /tmp

RUN apt update && \
    apt install --yes python3 python3-pip && \ 
    pip install -r /tmp/requirements.txt && \
    apt clean && \   
    rm -rf /var/lib/apt/lists/* && \
    rm -rf /root/.cache/ && \
    find /usr -depth -name __pycache__ -type d -exec rm -rf {} \;

COPY /macrophage /usr/lib/python3/dist-packages/macrophage
