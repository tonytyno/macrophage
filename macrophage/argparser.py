#


import argparse
from pathlib import Path
from macrophage.__init__ import __version__

DB_PATH = Path("data/lava.db")
LOG_FILE = Path("log.txt")
HTTP_RESPONSE = [404, 500]


def setup_parser() -> argparse.ArgumentParser:
    parent_parser = argparse.ArgumentParser(add_help=False)

    parser = argparse.ArgumentParser(
        prog="Macrophage",
        description="LAVA Crawler",
        epilog="Under Construction",
        parents=[parent_parser],
    )

    parent_parser.add_argument(
        "--log-file",
        type=Path,
        help="Set path for log file",
        default=LOG_FILE,
        action="store",
    )
    parent_parser.add_argument(
        "--log-level",
        "-l",
        type=str,
        help="Set log level to more specific information",
        default="info",
        choices=["debug", "info", "warn", "error"],
    )
    parent_parser.add_argument(
        "--db",
        type=Path,
        help="Set path for database",
        action="store",
        default=DB_PATH,
    )
    parent_parser.add_argument(
        "--summary",
        help="print database content summary",
        action="store_true",
    )
    parent_parser.add_argument(
        "--domains-in",
        help="print list of domains inside DB after crawling",
        action="store_true",
    )
    parent_parser.add_argument("--print", action="store_true", help="print results")
    subparsers = parser.add_subparsers(help="Choose Macrophage option")

    parser_crawler = subparsers.add_parser(
        "crawl",
        help="crawl whole website (fetch all content of website)",
        parents=[parent_parser],
    )
    parser_crawler.add_argument(
        "url", type=str, nargs="+", help="LAVA url for scraping", action="store"
    )
    parser_crawler.add_argument(
        "--config",
        type=int,
        nargs="+",
        help="set http responses status codes for LAVA crawler",
        action="store",
        default=HTTP_RESPONSE,
    )

    parser_link = subparsers.add_parser(
        "link", help="stored href overview", parents=[parent_parser]
    )
    parser_link.add_argument(
        "url", type=str, nargs="+", help="filter results by url", action="store"
    )
    parser_link.add_argument(
        "--config",
        type=int,
        nargs="+",
        help="filter results by http response status codes",
        action="store",
        default=HTTP_RESPONSE,
    )
    parser_link.add_argument(
        "--report-out",
        type=Path,
        help="set path for generated report",
        action="store",
        default=None,
    )

    parser_pattern = subparsers.add_parser(
        "search", help="search stored website content", parents=[parent_parser]
    )
    parser_pattern.add_argument(
        "url", type=str, nargs="+", help="filter stored content by url", action="store"
    )
    parser_pattern.add_argument(
        "word",
        type=str,
        help="the word you are looking for in LAVA instance",
        action="store",
        default=None,
    )
    parser_pattern.add_argument(
        "--report-out",
        type=Path,
        help="set path for generated report",
        action="store",
        default=None,
    )

    parser.add_argument(
        "-v",
        "--version",
        help="prints current version",
        action="version",
        version=f"%(prog)s, {__version__}",
    )
    return parser
