#

import yaml
import json
import csv
import logging


LOG = logging.getLogger("macrophage")


def generate_raport(raport_path, data) -> int:
    if raport_path is None:
        LOG.warning("** raport path is none, not able to save file")
        return 1
    if data is None:
        LOG.warning("** data is empty, not able to create file")
        return 1
    try:
        with open(raport_path, "a") as write_raport:
            if raport_path.suffix == ".yml" or raport_path.suffix == ".yaml":
                LOG.info(f"* Creating yaml file in {raport_path}")
                write_raport.write(yaml.dump(data))
            elif raport_path.suffix == ".csv":
                LOG.info(f"* Creating CSV file in {raport_path}")
                csv_writer = csv.DictWriter(write_raport, data[0].keys())
                csv_writer.writeheader()
                csv_writer.writerows(data)
            elif raport_path.suffix == ".json":
                LOG.info(f"* Creating JSON file in {raport_path}")
                write_raport.write(json.dumps(data))
            else:
                LOG.info(f"* Creating txt file in {raport_path}")
                raport_path.rename(raport_path.with_suffix(".txt"))
                for item in data:
                    for k, v in item.items():
                        write_raport.write(f"{str(k)}: {str(v)} ")
                    write_raport.write("\n")
        return 0
    except Exception as exc:
        LOG.warning(f"** Report not saved due to exception - {str(exc)}")
        return 1
