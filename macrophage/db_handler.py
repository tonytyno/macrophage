#
import logging
import sys
import sqlite3


LOG = logging.getLogger("macrophage")


def db_init_responses(db_path):
    db_con = sqlite3.connect(db_path)
    try:
        with db_con:
            cursor = db_con.cursor()
            cursor.execute(
                """CREATE TABLE IF NOT EXISTS link(
                id INTEGER NOT NULL PRIMARY KEY,
                url TEXT UNIQUE,
                status TEXT,
                domain TEXT
                );"""
            )
    except sqlite3.Error as exc:
        LOG.warning(f"*** Unable to create database for storing dead links! {str(exc)}")
        raise Exception("*** Macrophage execution stopped")
    LOG.info("* Making sure that db table for dead links is created")
    db_con.close()


def db_init_body(db_path):
    db_con = sqlite3.connect(db_path)
    try:
        with db_con:
            cursor = db_con.cursor()
            cursor.execute(
                """CREATE TABLE IF NOT EXISTS crawl (
                id INTEGER NOT NULL PRIMARY KEY,
                crawled TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
                url TEXT,
                source TEXT,
                domain TEXT
                );"""
            )
    except sqlite3.Error as exc:
        LOG.warning(
            f"*** Unable to create database for storing website content! {str(exc)}"
        )
        raise Exception("*** Macrophage execution stopped")
    LOG.info("* Making sure that db table for crawled website data is created")
    db_con.close()


def db_init_patterns(db_path):
    db_con = sqlite3.connect(db_path)
    try:
        with db_con:
            cursor = db_con.cursor()
            cursor.execute(
                """CREATE TABLE IF NOT EXISTS search (
                id INTEGER NOT NULL PRIMARY KEY,
                crawled TIMESTAMP,
                url TEXT,
                pattern TEXT,
                domain TEXT,
                UNIQUE (url, pattern)
                );"""
            )
    except sqlite3.Error as exc:
        LOG.warning(
            f"*** Unable to create database for storing searched input! {str(exc)}"
        )
        raise Exception("*** Macrophage execution stopped")
    LOG.info("* Making sure that db table for searched words is created")
    db_con.close()


def db_insert_response_value(db_path, url_value, status_value, domain):
    db_con = sqlite3.connect(db_path)
    try:
        with db_con:
            cursor = db_con.cursor()
            cursor.execute(
                "INSERT INTO link (url, status, domain) VALUES (?,?,?);",
                (url_value, status_value, domain),
            )
            LOG.debug(
                f"Inserted url: {url_value} with code: {status_value} to database"
            )
    except sqlite3.IntegrityError:
        LOG.info(f"* Value for {url_value} already existing in db!")
    db_con.close()


def db_insert_body_value(db_path, query_list):
    db_con = sqlite3.connect(db_path)
    try:
        with db_con:
            cursor = db_con.cursor()
            cursor.executemany(
                "INSERT OR REPLACE INTO crawl (url, source, domain) VALUES (?,?,?);",
                query_list,
            )
            LOG.debug(f"* Inserted url: {query_list[0]} content to database")
    except sqlite3.Error:
        LOG.info(f"* Value for {query_list[0]} already existing in db!")
    db_con.close()


def dict_factory(cursor, row):
    col_names = [col[0] for col in cursor.description]
    return {key: value for key, value in zip(col_names, row)}


def db_dict_urls(db_path, domains):
    LOG.debug("* Creating result list of link table")
    if len(domains) == 1:
        domains = f"('{domains[0]}')"
    result_list = list()
    db_con = sqlite3.connect(db_path)
    try:
        with db_con:
            db_con.row_factory = dict_factory
            for row in db_con.execute(f"SELECT * FROM link WHERE domain IN {domains};"):
                result_list.append(row)
    except sqlite3.Error as exc:
        LOG.warning(f"** Not able to retrieve dead links from database! {str(exc)}")
        raise Exception("*** Macrophage execution stopped")
    db_con.close()
    return result_list


def db_pattern_find(db_path, pattern, domains):
    LOG.info(f"** Starting word search for: {pattern}")
    result_list = list()
    sql_pattern = f"%{pattern}%"
    if len(domains) == 1:
        domains = f"('{domains[0]}')"
    db_con = sqlite3.connect(db_path)
    try:
        with db_con:
            db_con.row_factory = dict_factory
            for row in db_con.execute(
                f"SELECT url, crawled, domain FROM crawl WHERE source LIKE '{sql_pattern}' AND domain IN {domains};"
            ):
                row["pattern"] = pattern
                result_list.append(row)

                LOG.info(
                    f'*** Word: {pattern} found in {row["url"]} - value inserted to database'
                )
            db_con.executemany(
                "INSERT OR IGNORE INTO search (url, crawled, domain, pattern) VALUES (:url, :crawled, :domain, :pattern);",
                result_list,
            )
    except sqlite3.Error as exc:
        LOG.warning(
            f"*** Not able to search through database for selected word: {pattern}, {str(exc)}"
        )
        raise Exception(
            f"*** Not able to search through database for selected word: {pattern}, {str(exc)}"
        )
    db_con.close()
    return result_list


def db_print_results(
    db_path, table_name, domains, status_filter, searched_pattern=None
):
    status_filter = tuple(status_filter)
    if len(domains) == 1:
        domains = f"('{domains[0]}')"
    if len(status_filter) == 1:
        status_filter = f"('{status_filter[0]}')"
    sql_query = f"SELECT * FROM {table_name} WHERE domain IN {domains} AND status IN {status_filter};"
    if searched_pattern is not None:
        sql_query = f"SELECT * FROM {table_name} WHERE pattern = '{searched_pattern}' AND domain IN {domains};"
    db_con = sqlite3.connect(db_path)
    try:
        with db_con:
            cursor = db_con.cursor()
            result = cursor.execute(sql_query)
            result_list = result.fetchall()
    except sqlite3.Error as exc:
        LOG.warning(f"*** Not able to retrieve data from database! {str(exc)}")
        raise Exception("*** Macrophage execution stopped")
    db_con.close()

    i = 0
    for line in result_list:
        if searched_pattern is None:
            sys.stdout.write(f"url: {line[1]} http response: {line[2]}\n")
        else:
            sys.stdout.write(
                f"url: {line[2]}, found date: {line[1]}, searched pattern: {line[3]}\n"
            )
        i += 1
        if i == 19:
            i = 0
            input("press any key for next 20 records")


def db_print_summary(db_path, table, domains):
    if len(domains) == 1:
        domains = f"('{domains[0]}')"
    db_con = sqlite3.connect(db_path)
    try:
        with db_con:
            cursor = db_con.cursor()
            result = cursor.execute(
                f"SELECT COUNT(*) FROM {table} WHERE domain IN {domains};"
            )
            sys.stdout.write(
                f"** Table {table} contains {result.fetchone()[0]} records\n"
            )
    except sqlite3.Error as exc:
        LOG.warning(f"*** Not able to retrieve data from database! {str(exc)}")
        raise Exception("*** Macrophage execution stopped")
    db_con.close()


def db_print_domain_list(db_path):
    db_con = sqlite3.connect(db_path)
    try:
        with db_con:
            cursor = db_con.cursor()
            result = cursor.execute("SELECT DISTINCT domain FROM crawl;")
            for domain in result.fetchall():
                sys.stdout.write(f"** Table contains: {domain[0]}\n")
    except sqlite3.Error as exc:
        LOG.warning(f"*** Not able to retrieve data from database! {str(exc)}")
        raise Exception("*** Macrophage execution stopped")
    db_con.close()
