#
# Copyright 2022-present Linaro Limited
#
# SPDX-License-Identifier: MIT
import sys
import logging.handlers
import contextlib

from scrapy.crawler import CrawlerProcess
from macrophage.crawler import (
    CrawlingSpider,
    trim_allowed_domains,
    trim_urls_to_domains,
)
from macrophage.db_handler import (
    db_init_patterns,
    db_init_body,
    db_init_responses,
    db_dict_urls,
    db_pattern_find,
    db_print_summary,
    db_print_results,
    db_print_domain_list,
)
from macrophage.argparser import setup_parser
from macrophage.raport_creator import generate_raport

LOG = logging.getLogger("macrophage")


def main() -> int:
    parser = setup_parser()
    options = parser.parse_args()
    argument = None
    with contextlib.suppress(IndexError):
        argument = sys.argv[1]
    if argument in ["link", "search", "crawl"]:
        LOG.info(f"** Macrophage mode {sys.argv[1]} started")
        log_handler = logging.StreamHandler(sys.stdout)
        log_handler.setFormatter(logging.Formatter("%(message)s"))
        log_file_handler = logging.handlers.WatchedFileHandler(
            (options.log_file).absolute(),
            mode="w",
        )
        LOG.addHandler(log_handler)
        LOG.addHandler(log_file_handler)

        if options.log_level == "debug":
            LOG.setLevel(logging.DEBUG)
        elif options.log_level == "warn":
            LOG.setLevel(logging.WARNING)
        elif options.log_level == "error":
            LOG.setLevel(logging.ERROR)
        else:
            LOG.setLevel(logging.INFO)

        db_path = (options.db).absolute()
        db_path.parent.mkdir(exist_ok=True)
        LOG.debug(f"* Database path set to: {db_path}")
        searched_pattern = None
        if sys.argv[1] == "link":
            link_filter = options.config
        if sys.argv[1] == "search":
            searched_pattern = options.word
            link_filter = []
            LOG.debug(f"* Searched word set to {searched_pattern}")
        spider_kwargs = {
            "db_path": db_path,
            "start_urls": options.url,
            "domains": trim_urls_to_domains(options.url),
            "allowed_domains": trim_allowed_domains(options.url),
        }
        if options.summary:
            db_print_summary(db_path, sys.argv[1], spider_kwargs["domains"])
            return 0
        if options.domains_in:
            db_print_domain_list(db_path)
            return 0
        if (sys.argv[1] == "link" and options.report_out is None) or options.print:
            db_print_results(
                db_path,
                sys.argv[1],
                spider_kwargs["domains"],
                link_filter,
                searched_pattern,
            )
            return 0
        if options.url and sys.argv[1] == "crawl":
            db_init_body(db_path)
            db_init_responses(db_path)
            spider_kwargs["response_list"] = options.config
            spider_kwargs["handle_httpstatus_list"] = options.config
            LOG.info(f"Crawling started for {options.url}")
            lava_spider = CrawlerProcess({"JOBDIR": "./job"})
            lava_spider.crawl(CrawlingSpider, **spider_kwargs)
            lava_spider.start()
            LOG.info("Crawling ended")
        elif options.url and sys.argv[1] == "link":
            if options.report_out is not None:
                report_file_path = (options.report_out).absolute()
                report_file_path.parent.mkdir(exist_ok=True)
                generate_raport(
                    report_file_path, db_dict_urls(db_path, spider_kwargs["domains"])
                )
        elif options.url and sys.argv[1] == "search":
            db_init_patterns(db_path)
            db_dict_pattern = db_pattern_find(
                db_path, searched_pattern, spider_kwargs["domains"]
            )
            if options.report_out is not None:
                report_file_path = (options.report_out).absolute()
                report_file_path.parent.mkdir(exist_ok=True)
                generate_raport(report_file_path, db_dict_pattern)
    else:
        parser.print_help()
    return 0


def start() -> None:
    if __name__ == "__main__":
        sys.exit(main())


start()
