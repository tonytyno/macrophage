import logging
from scrapy import signals
from pydispatch import dispatcher
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor

from macrophage.db_handler import (
    db_insert_response_value,
    db_insert_body_value,
)
from urllib.parse import urlparse
from typing import List, Any


LOG = logging.getLogger("macrophage")


class CrawlingSpider(CrawlSpider):
    sql_cache = list()  # type: List[Any]
    name = "crawl"
    link_extractor = LinkExtractor(
        deny=[
            r"https?:\/\/.+\/accounts\/log..?.\/\?next=\/.+",
            r"^https?:\/\/.+\/.+\?(dt_)?page=([4-9][0-9]|\d{3,})(.?)+",
            r"https?:\/\/.+\/.+\/.+\/.+\?device_log_page=.+&.+",
            r"https?:\/\/.+\/.+\/.+sort=.+",
            r"^https?:\/\/.+?ordering=.+$",
            r"https?:\/\/.+\/configuration$",
        ]
    )
    rules = [Rule(link_extractor, callback="crawl_parse", follow=True)]

    custom_settings = {
        "LOG_LEVEL": "INFO",
    }

    def __init__(self, db_path, response_list, *args, **kwargs):
        self.db_path = db_path
        self.response_list = response_list
        super(CrawlingSpider, self).__init__(*args, **kwargs)
        dispatcher.connect(self.spider_closed, signals.spider_closed)

    def spider_closed(self, spider):
        LOG.info("** Finishing execution and inserting value to Database")
        db_insert_body_value(self.db_path, self.sql_cache)
        LOG.debug(f"* cache size: {len(self.sql_cache)}")
        self.sql_cache.clear()
        LOG.debug(f"* cache size: {len(self.sql_cache)}")

    def crawl_parse(self, response):
        domain = urlparse(response.url)
        if response.status in self.response_list:
            LOG.debug(
                f"** Wanted Link has been found url: {response.url} status code: {response.status}"
            )
            db_insert_response_value(
                self.db_path,
                response.url,
                response.status,
                domain.netloc,
            )
        else:
            LOG.debug(f"{response.url} scraped")
            try:
                self.sql_cache.append(
                    (response.url, response.body.decode("utf-8"), domain.netloc)
                )
            except UnicodeDecodeError as exc:
                LOG.warning(
                    f"Can't decode response for url: {response.url}, data won't be present in database. Exception details: {str(exc)}"
                )

            LOG.debug(f"* cache size: {len(self.sql_cache)}")
            if len(self.sql_cache) >= 250:
                LOG.info(f"*** Still crawling through {domain.netloc}")
                LOG.debug("* Cache full, inserting to Database")
                db_insert_body_value(self.db_path, self.sql_cache)
                self.sql_cache.clear()
                LOG.debug(f"* cache size: {len(self.sql_cache)}")


def trim_allowed_domains(domains: list) -> list:
    if domains is None:
        LOG.warning("* Domain list is empty")
        raise Exception("Domain list is empty")
    allowed_domains = list()
    for domain in domains:
        url = urlparse(domain).netloc
        if ":" in domain:
            domain = (url.partition(":"))[0]
        allowed_domains.append(domain)
    return allowed_domains


def trim_urls_to_domains(start_urls: list) -> tuple:
    if start_urls is None:
        LOG.warning("* Url list is empty")
        raise Exception("Url list is empty")
    domains = list()
    for url in start_urls:
        url = urlparse(url).netloc
        domains.append(url)
    return tuple(domains)
